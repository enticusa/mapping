import sys
import os
import openpyxl
from openpyxl.cell import Cell
from openpyxl.worksheet import worksheet
from openpyxl.styles import PatternFill, Alignment, Border, Side, Font
from PyQt5.QtCore import pyqtSlot, pyqtSignal
from PyQt5.QtWidgets import QApplication, \
                            QWidget, \
                            QInputDialog, \
                            QLineEdit, \
                            QFileDialog, \
                            QPushButton, \
                            QHBoxLayout, \
                            QVBoxLayout, \
                            QMessageBox
from PyQt5.QtGui import QIcon
import dropbox
import traceback
import tempfile
from typing import Optional

DROPBOX_TOKEN = "rDTtv9X5-DUAAAAAAABS1wRip4bbJv99pa7xWbMWBw5DdWF9uDaXhzZHQWdH40xD"


# Takes a sheet or a cell with modifiers and return the modified cell
def modify_cell(sheet: worksheet = None,
                cell: Cell = None,
                row: int = None,
                column: int = None,
                value=None,
                patternfill: Optional[PatternFill] = None,
                border: Optional[Border] = None,
                alignment: Optional[Alignment] = None,
                font: Optional[Font] = None
                ) -> Cell:

    if sheet is None and cell is None:
        print("No valid object to write")

    else:
        if sheet is not None and row is not None and column is not None and value is not None:
            modified_cell = sheet.cell(row=row, column=column, value=value)
        elif cell is not None and value is not None:
            modified_cell = cell
            modified_cell.value = value
        else:
            modified_cell = cell

        if patternfill is not None:
            modified_cell.fill = patternfill

        if border is not None:
            modified_cell.border = border
        else:
            # If no border is specified, just add a border because it's ugly without it
            modified_cell.border = Border(left=Side(border_style="medium", color='00000000'),
                                          right=Side(border_style="medium", color='00000000'),
                                          top=Side(border_style="medium", color='00000000'),
                                          bottom=Side(border_style="medium", color='00000000'))
        if alignment is not None:
            modified_cell.alignment = alignment
        if font is not None:
            modified_cell.font = font

        return modified_cell


# Checks if the sheet contains a specific column header that makes it valid
def check_if_master_list_sheet_is_valid(sheet):
    isvalid = False
    for row in sheet.iter_rows(min_row=1, max_row=1, min_col=1, max_col=4):
        for cell in row:
            if cell.value == "Prefixes":
                isvalid = True

    return isvalid


def style_sheet(sheet=None):
    for col in sheet.columns:
        max_length = 0
        for cell in col:
            if cell.value != 1:

                # Yellow fill with thick borders
                modify_cell(cell=cell,
                            patternfill=PatternFill(start_color='FFFF00',
                                                    end_color='FFFF00',
                                                    fill_type='solid'),
                            border=Border(left=Side(border_style="medium", color='00000000'),
                                          right=Side(border_style="medium", color='00000000'),
                                          top=Side(border_style="medium", color='00000000'),
                                          bottom=Side(border_style="medium", color='00000000')))

            try:
                if len(str(cell.value)) > max_length:
                    max_length = len(cell.value)
            except:
                pass

        #TODO Broke when updated to Python 3.7
        #adj_size = max_length
        #sheet.column_dimensions[column].width = str(adj_size)


# Pass the column index and min-max rows
def generate_coordinates(sheet: worksheet,
                         column_idx: int,
                         min_row: int,
                         max_row: int):

    cell1 = sheet.cell(row=min_row, column=column_idx)
    cell2 = sheet.cell(row=max_row, column=column_idx)
    first_letter = cell1.coordinate
    second_letter = cell2.coordinate
    return first_letter, second_letter


class QaParser(QWidget):
    def __init__(self):
        super().__init__()
        # UI Init
        self.title = "Mapping loader"
        self.left = 10
        self.top = 10
        self.width = 400
        self.height = 80

        # file paths
        self.master_filepath = ''
        self.devicedict = {}
        self.pointcount = 0
        self.book = None
        self.book_master = None
        self.map_association = {}
        self.reference = {}

        self.dialog = QMessageBox()
        self.init_ui()

    def init_ui(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)

        layout = QHBoxLayout()

        load_mapping_button = QPushButton("Load Mapping File", self)
        load_mapping_button.clicked.connect(self.get_mapping_file)

        save_report_button = QPushButton("Save Report", self)
        save_report_button.clicked.connect(self.save_book)

        layout.addWidget(load_mapping_button)
        layout.addWidget(save_report_button)

        self.setLayout(layout)
        self.show()

    def error_message_popup(self, message):
        self.dialog.setWindowTitle("Critical error")
        self.dialog.setIcon(QMessageBox.Critical)
        self.dialog.setText("{}\n\n\nExiting now..".format(message))
        self.dialog.exec_()
        sys.exit(1)

    def success_message_popup(self, message):
        self.dialog.setWindowTitle("Information")
        self.dialog.setIcon(QMessageBox.Information)
        self.dialog.setText(message)
        self.dialog.exec_()

    # Downloads pathname file from Dropbox
    # Returns an excel binary ready to be open by openpyxl
    def get_excel_from_dropbox(self, pathname):
        dbx = dropbox.Dropbox(DROPBOX_TOKEN)
        try:
            result = dbx.files_list_folder(pathname)
            for entry in result.entries:
                if "Parseable" in entry.name:
                    filepath = entry.path_lower
                    try:
                        md, res = dbx.files_download(filepath)
                        temp_file = tempfile.NamedTemporaryFile()
                        temp_file.write(res.content)
                        temp_file.seek(0)
                        return temp_file
                    except Exception as exc:
                        self.error_message_popup("Exception occurred with code: '{}'".format(exc))
                        traceback.print_exc()

        except Exception as exc:
            self.error_message_popup("Exception occurred with code: '{}'".format(exc))
            traceback.print_exc()

        return None

    @pyqtSlot()
    def get_mapping_file(self):
        try:
            qfd = QFileDialog()
            options = QFileDialog.options(qfd)
            file_path, _ = QFileDialog.getOpenFileName(qfd,
                                                       'Load mapping file', '',
                                                       "Excel files (*.xls *.xlsx)",
                                                       options=options)

            self.parse_master_list()
            self.parse_excel(file_path)
            self.modify_workbook()
            self.success_message_popup("New workbook created, please save the report")

        except Exception as exc:
            self.error_message_popup("Exception occurred with code: '{}'".format(exc.args))
            traceback.print_exc()

    def save_book(self):
        qfd = QFileDialog()
        options = QFileDialog.options(qfd)
        filename, _ = QFileDialog.getSaveFileName(qfd,
                                                  'Save mapping file', '',
                                                  "Excel files (*.xls *.xlsx)",
                                                  options=options)
        self.book.save(filename)
        self.success_message_popup("Book saved successfully to location\n{}".format(filename))
        sys.exit(0)

    def parse_master_list(self):
        file = self.get_excel_from_dropbox("/Engineering/Standards/MAPPING")
        self.book_master = openpyxl.load_workbook(file, data_only=True, read_only=True)
        for sheet in self.book_master.worksheets:
            if check_if_master_list_sheet_is_valid(sheet):
                # Pull the prefixes as keys of the dictionary
                for row in sheet.iter_rows(min_col=3, max_col=3, min_row=2):
                    for cell in row:
                        if cell.value is None or cell.value == "":
                            break
                        self.reference[cell.value] = []
                        # Fill each tuple key with the corresponding points
                        for _row in sheet.iter_rows(min_col=2, max_col=2, min_row=2):
                            for _cell in _row:
                                if _cell.value == "" or _cell.value is None:
                                    continue
                                else:
                                    self.reference[cell.value].append(_cell.value)

    def parse_excel(self, file_path):
        with open(file_path, 'rb') as f:
            self.book = openpyxl.load_workbook(f, data_only=True)
            main_sheet = self.book['export']
            for row in main_sheet.iter_rows(min_row=2, min_col=4, max_col=4):
                for cell in row:
                    if cell.value is not None:
                        self.pointcount += 1
                        device_type = cell.value.split("_")[0]
                        identifier = cell.value.split("_")[1]
                        pointname = cell.value.split("_")[2]
                        if device_type not in self.devicedict:
                            self.devicedict[device_type] = {}
                            self.devicedict[device_type][identifier] = []
                            self.devicedict[device_type][identifier].append(pointname)
                        else:
                            if identifier not in self.devicedict[device_type]:
                                self.devicedict[device_type][identifier] = []
                                self.devicedict[device_type][identifier].append(pointname)
                            else:
                                self.devicedict[device_type][identifier].append(pointname)

                        if device_type not in self.map_association:
                            self.map_association[device_type] = []
                        else:
                            if pointname not in self.map_association[device_type]:
                                self.map_association[device_type].append(pointname)

    def modify_workbook(self):
        for device, identifier in self.devicedict.items():
            max_row = len(self.map_association[device])
            sheet = self.book.create_sheet()
            sheet.title = device

            if isinstance(identifier, dict):

                # A1 value is the identifier
                a1 = "{} Identifiers".format(device)

                modify_cell(sheet=sheet,
                            row=1,
                            column=1,
                            value=a1,
                            alignment=Alignment(horizontal='center',
                                                vertical='center',
                                                textRotation=45),
                            font=Font(name='Consolas',
                                      size=14,
                                      color='00000000'))

                modify_cell(sheet=sheet,
                            row=2,
                            column=1,
                            value="Type",
                            font=Font(name='Consolas',
                                      size=14,
                                      color='00000000'))

                # Make the first row float
                sheet.freeze_panes = 'A2'

                # Populates column 1 with every point name \
                # associated with the device that was mapped
                # If there is a typo, it will populate too

                for idx, name in enumerate(self.map_association[device]):
                    for i_d, points in self.reference.items():
                        if i_d == device:
                            if name not in points:
                                modify_cell(sheet=sheet,
                                            row=idx + 3,
                                            column=1,
                                            value=name,
                                            font=Font(name='Consolas',
                                                      size=14,
                                                      color='FA8072'),
                                            )
                            else:
                                modify_cell(sheet=sheet,
                                            row=idx + 3,
                                            column=1,
                                            value=name,
                                            font=Font(name='Consolas',
                                                      size=14,
                                                      color='00000000'))

                for idx, (id_name, p_names) in enumerate(sorted(identifier.items())):

                    # Write the identifier
                    # Make the identifier cell rotated 45 degrees.
                    # Make the font Consolas for clarity

                    modify_cell(sheet=sheet,
                                row=1,
                                column=idx+2,
                                value=id_name,
                                alignment=Alignment(horizontal='general',
                                                    vertical='center',
                                                    textRotation=45),
                                font=Font(name='Consolas',
                                          size=16,
                                          color='00000000'))

                    # Loop across each point name in identifier.items()
                    # Mark each mapped point with a one.
                    for name in p_names:
                        for col in sheet.iter_cols(min_row=3, min_col=idx+2, max_col=idx+2):
                            for i, cell in enumerate(col):
                                cl = sheet.cell(row=i+3, column=1)
                                if name == cl.value:
                                    modify_cell(cell=cell, value=1)
                                    for i_d, points in self.reference.items():
                                        if i_d == device:
                                            if name in points:
                                                modify_cell(cell=cell,
                                                            patternfill=PatternFill(start_color='90EE90',
                                                                                    end_color='90EE90',
                                                                                    fill_type='solid'))
                                            else:
                                                modify_cell(cell=cell,
                                                            patternfill=PatternFill(start_color='FA8072',
                                                                                    end_color='FA8072',
                                                                                    fill_type='solid'))

                    x, y = generate_coordinates(sheet=sheet, column_idx=idx+2, min_row=3, max_row=max_row+2)
                    formula = "=SUM({}:{})".format(x, y)
                    modify_cell(sheet=sheet, row=max_row + 3, column=idx+2, value=formula)
                style_sheet(sheet)


if __name__ == "__main__":
    ui_app = QApplication(sys.argv)
    icon_path = os.path.join(os.path.dirname(sys.argv[0]), 'map_icon.png')
    icon = QIcon(icon_path)
    ui_app.setWindowIcon(icon)

    qaparser = QaParser()
    sys.exit(ui_app.exec_())
    



